var express = require('express');
var axios = require('axios');
var router = express.Router();

/*
HTTP Method 
GET : select
POST : insert, data need to be protected, like login
PUT : update
DELETE : delete

Passing params:
Query String : url http://domain:port/movie?key=korea
form : key
home page. */
router.get('/api', function (req, res, next) {
  res.json({
    name: "keople"
  });
});


router.get('/naver', function (req, res, next) {

  axios.get("http://www.naver.com")
    .then(response => {
      console.log(response);
      res.send("ok")
    })
    .catch(err => {
      console.log(err);
      res.send("err")
    })

});

router.get('/api/:name', function (req, res, next) {
  res.json({
    message: `Your name is ${req.params.name}`
  });
});

router.post('/api/', (req, res, next) => {
  res.json({
    message: `Your name is ${req.body.name}`
  });
});

router.put('/api/', (req, res, next) => {
  res.json({
    message: `Your name is not ${req.body.name}`
  });
});

module.exports = router;